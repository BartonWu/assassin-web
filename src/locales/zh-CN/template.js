export default {
  'assassin.template': '模版',
  'assassin.template.list': '模版列表',
  'assassin.template.name': '模版名称',
  'assassin.template.modal.name.valid': '请输入模版名称',
  'assassin.template.access': '分配权限',
};
